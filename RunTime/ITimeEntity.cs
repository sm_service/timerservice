﻿using System;

namespace mce.TimerService
{
    public interface ITimeEntity
    {
        public event Action OnEnd;
        public event Action OnTimerUpdate;
        public int TimeToEnd { get; }
        public int AllTime { get; }
        public bool IsEnd { get; }
        public int CountEnded { get; }
    }
}