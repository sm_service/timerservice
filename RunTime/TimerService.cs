﻿using System;
using System.Collections.Generic;
using System.Linq;
using mce.LocalSave;
using UnityEngine;

namespace mce.TimerService
{
    public class TimerService
    {
        //TODO нужно будет вынести в SO
        private const int TickToSecond = 1;
        private const int TickToSave = 15;

        private readonly LocalSaveService _localSave;

        private List<TimerEntity> _entities;
        private float _tickToSecond;
        private float _tickToSave;

        public TimerService(LocalSaveService localSaveService)
        {
            _localSave = localSaveService;
            _entities = new List<TimerEntity>();

            LoadSave();
        }

        public void Tick()
        {
            if (_entities.Count == 0)
            {
                return;
            }

            _tickToSecond += Time.deltaTime;
            _tickToSave += Time.deltaTime;
            bool isNeedRemove = false;
            if (_tickToSecond >= TickToSecond)
            {
                foreach (var entity in _entities)
                {
                    if (!entity.IsEnd)
                    {
                        entity.SetTime(entity.TimeToEnd - 1);
                    }

                    if (entity.IsNeedRemove && entity.IsNeedSave)
                    {
                        isNeedRemove = true;
                    }
                }

                _tickToSecond = 0;
            }

            if (isNeedRemove)
            {
                _entities.RemoveAll(x => x.IsNeedRemove);
            }

            if (_tickToSave >= TickToSave)
            {
                SaveData();
                _tickToSave = 0;
            }
        }

        //TODO нужно будет подумать об замене string to int
        public void ResetTimer(string timerName)
        {
            var timers = _entities.Where(x => x.TimerName == timerName).ToList();
            if (timers.Count == 0)
            {
                Debug.LogError($"[Timer] не найдено таймеров с именем {timerName}");
                return;
            }

            if (timers.Count > 1)
            {
                Debug.LogError($"[Timer] найдено {timers.Count} таймеров с именем {timerName}");
                return;
            }

            timers[0].SetTime(timers[0].AllTime);
        }

        public void RemoveTimer(string timerName)
        {
            var timers = _entities.Where(x => x.TimerName == timerName).ToList();
            if (timers.Count == 0)
            {
                Debug.LogError($"[Timer] не найдено таймеров с именем {timerName}");
                return;
            }

            if (timers.Count > 1)
            {
                Debug.LogError($"[Timer] найдено {timers.Count} таймеров с именем {timerName}");
                return;
            }

            if (timers[0].IsNeedRemove)
            {
                Debug.LogError($"[Timer] с именем {timerName} уже помечен для удаления");
                return;
            }

            timers[0].IsNeedRemove = true;
        }

        public ITimeEntity GetTimer(string timerName)
        {
            var timers = _entities.Where(x => x.TimerName == timerName).ToList();
            if (timers.Count == 0)
            {
                return null;
            }

            if (timers.Count > 1)
            {
                Debug.LogError($"[Timer] найдено {timers.Count} таймеров с именем {timerName}");
                return null;
            }

            return timers[0];
        }

        public ITimeEntity CreateTimer(string timerName, int timeToEnd, bool isNeedSave, bool isTickOffline)
        {
            var isHave = _entities.Any(x => x.TimerName == timerName);
            if (isHave)
            {
                Debug.LogError($"[Timer] Have time with name {timerName}");
                return null;
            }

            var entity = new TimerEntity(timerName, timeToEnd, timeToEnd, isNeedSave, isTickOffline);
            _entities.Add(entity);
            SaveData();
            return entity;
        }

        private void SaveData()
        {
            var timersData = new TimersData();
            foreach (var entity in _entities)
            {
                if (entity.IsNeedRemove)
                {
                    continue;
                }

                if (!entity.IsNeedSave)
                {
                    continue;
                }

                var data = new TimerEntityData();
                data.TimerName = entity.TimerName;
                data.TimeToEnd = entity.TimeToEnd;
                data.AllTime = entity.AllTime;
                data.IsTickOffline = entity.IsTickOffline;
                timersData.Entities.Add(data);
            }

            timersData.LastGameEndTime = DateTime.Now.Ticks;
            _localSave.SaveData<TimersData>(timersData);
        }

        private void LoadSave()
        {
            var timersData = _localSave.GetData<TimersData>();

            if (timersData == null)
                return;
            var deltaTime = DateTime.Now - new DateTime(timersData.LastGameEndTime);
            int offlineTimeSeconds = (int) deltaTime.TotalSeconds;
            if (offlineTimeSeconds <= 0)
                offlineTimeSeconds = 0;

            Debug.Log($"[Timer] offlineTimeSeconds {offlineTimeSeconds}");
            foreach (var entityData in timersData.Entities)
            {
                var timeToEnd = entityData.TimeToEnd;
                int countEnded = 0;
                if (entityData.IsTickOffline)
                {
                    timeToEnd -= offlineTimeSeconds;
                }

                if (timeToEnd <= 0)
                {
                    countEnded = -timeToEnd / entityData.AllTime;
                    countEnded = countEnded + 1;
                    timeToEnd = 0;
                }

                var entity = new TimerEntity(entityData.TimerName, timeToEnd, entityData.AllTime, true,
                    entityData.IsTickOffline);
                entity.CountEnded = countEnded;
                _entities.Add(entity);
            }
        }
    }
}