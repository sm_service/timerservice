﻿using System;

namespace mce.TimerService
{
    public class TimerEntity : ITimeEntity
    {
        public event Action OnEnd;
        public event Action OnTimerUpdate;
        public int AllTime { get; }
        public string TimerName { get; }
        public bool IsNeedSave { get; }
        public bool IsTickOffline { get; }


        public bool IsNeedRemove { get; set; }
        public int CountEnded { get; set; }
        public int TimeToEnd { get; private set; }
        public bool IsEnd => TimeToEnd <= 0;

        public TimerEntity(string timerName, int timeToEnd, int allTime, bool isNeedSave, bool isTickOffline)
        {
            TimerName = timerName;
            IsNeedSave = isNeedSave;
            IsTickOffline = isTickOffline;

            AllTime = allTime;
            TimeToEnd = timeToEnd;
        }


        public void SetTime(int newTime)
        {
            TimeToEnd = newTime;
            OnTimerUpdate?.Invoke();
            if (TimeToEnd <= 0)
            {
                OnEnd?.Invoke();
            }
        }
    }
}