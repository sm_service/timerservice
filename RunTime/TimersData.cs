﻿using System;
using System.Collections.Generic;
using mce.LocalSave;
using UnityEngine.Serialization;

namespace mce.TimerService
{
    [Serializable]
    public class TimersData : LocalData
    {
        public long LastGameEndTime;
        public List<TimerEntityData> Entities = new List<TimerEntityData>();
    }
    
    [Serializable]
    public class TimerEntityData
    {
        public string TimerName;
        public int TimeToEnd;
        public int AllTime;
        public bool IsTickOffline;
    }
}